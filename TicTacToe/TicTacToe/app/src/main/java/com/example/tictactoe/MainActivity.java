package com.example.tictactoe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btns[][] = new Button[3][3];
    //private Button NewGame_btn;
    //private TextView PlayerTurns;
    private boolean playerXTurn = true;
    private int roundcount;
    private TextView TurnState;
    private String WinnerName;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.New_Game_menu) {
            //process your onClick here
            RestBoard();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // NewGame_btn = (Button) findViewById(R.id.NewGame);
        // PlayerTurns = (TextView) findViewById(R.id.playerTurn);
        TurnState = (TextView) findViewById(R.id.TurnsState);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                String ButtonID = "button" + i + j;
                int resID = getResources().getIdentifier(ButtonID, "id", getPackageName());
                btns[i][j] = findViewById(resID);
                btns[i][j].setOnClickListener(this);
            }
        }
//        NewGame_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//            RestBoard();
//            }
//        });
    }

    @Override
    public void onClick(View v) {
        if (!((Button) v).getText().toString().equals("")) {
            return;
        }
        if (playerXTurn) {
            ((Button) v).setText("X");
            // PlayerTurns.setText("It's O Player Turn");
            TurnState.setText("It's O Player Turn");
        } else {
            ((Button) v).setText("O");
            //PlayerTurns.setText("It's X Player Turn");
            TurnState.setText("It's X Player Turn");
        }
        roundcount++;

        if (CheckWin()) {
            if (playerXTurn) {
                playerXWin();
            } else {
                playerOWin();
            }
        } else if (roundcount == 9) {
            drwa();
        } else {
            playerXTurn = !playerXTurn;
        }
    }

    private void playerXWin() {
        //PlayerTurns.setText("X Wins");
        //TurnState.setText("X Wins");
        //Toast.makeText(this, "Player 1 or 'X' Wins", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, WinnerScreenActivity.class);

//Create the bundle
        Bundle bundle = new Bundle();
//Add your data to bundle
        bundle.putString("Winner", "Player 1 or 'X' Wins");
//Add the bundle to the intent
        i.putExtras(bundle);
//Fire that second activity
        startActivity(i);
        RestBoard();
    }


    private void playerOWin() {
        //TurnState.setText("O Wins");
        //Toast.makeText(this, "Player 2 or 'O' Wins", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, WinnerScreenActivity.class);

//Create the bundle
        Bundle bundle = new Bundle();
//Add your data to bundle
        bundle.putString("Winner", "Player 2 or 'O' Wins");
//Add the bundle to the intent
        i.putExtras(bundle);
//Fire that second activity
        startActivity(i);
        // PlayerTurns.setText("O Wins");
        RestBoard();
    }

    private void drwa() {
        //TurnState.setText("Its Draw");
      //  Toast.makeText(this, "Its Draw", Toast.LENGTH_SHORT).show();

        Intent i = new Intent(this, WinnerScreenActivity.class);

//Create the bundle
        Bundle bundle = new Bundle();
//Add your data to bundle
        bundle.putString("Winner", "Its Draw");
//Add the bundle to the intent
        i.putExtras(bundle);
//Fire that second activity
        startActivity(i);

        RestBoard();
    }

    private void RestBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                btns[i][j].setText("");
            }
        }
        roundcount = 0;
        playerXTurn = true;
        // PlayerTurns.setText("It's X Player Turn");
        TurnState.setText("It's X Player Turn");

    }

    private Boolean CheckWin() {
        String btnfields[][] = new String[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                btnfields[i][j] = btns[i][j].getText().toString();
            }
        }
        for (int i = 0; i < 3; i++) {
            //checking all rows
            if (btnfields[i][0].equals(btnfields[i][1])
                    && btnfields[i][0].equals(btnfields[i][2])
                    && !btnfields[i][0].equals("")) {
                return true;
            }
        }
        for (int i = 0; i < 3; i++) {
            //checking all clomns
            if (btnfields[0][i].equals(btnfields[1][i])
                    && btnfields[0][i].equals(btnfields[2][i])
                    && !btnfields[0][i].equals("")) {
                return true;
            }
        }
        //checking all dianagol
        if (btnfields[0][2].equals(btnfields[1][1])
                && btnfields[0][2].equals(btnfields[2][0])
                && !btnfields[0][2].equals("")) {
            return true;
        }
        //checking all dianagol
        if (btnfields[0][0].equals(btnfields[1][1])
                && btnfields[0][0].equals(btnfields[2][2])
                && !btnfields[0][0].equals("")) {
            return true;
        }
        return false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putString("PlayerTurnK",PlayerTurns.getText().toString());
        outState.putString("TurnStateK", TurnState.getText().toString());
        outState.putInt("RoundCountK", roundcount);
        outState.putBoolean("PlayerXTurnK", playerXTurn);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        roundcount = savedInstanceState.getInt("RoundCountK");
        playerXTurn = savedInstanceState.getBoolean("PlayerXTurnK");
        // PlayerTurns.setText(savedInstanceState.getString("PlayerTurnK"));
        TurnState.setText(savedInstanceState.getString("TurnStateK"));
    }
}
